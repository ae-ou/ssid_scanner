# SSID Scanner

This module is used to scan for nearby Wireless Access points on Linux machines.

# Requirements

- Linux-based OS (e.g. Ubuntu 18.04)
- iw
- iwlist

## License

Creative Commons Attribution Share Alike 4.0 International