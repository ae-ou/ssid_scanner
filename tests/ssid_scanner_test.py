import unittest
from ssid_scanner import ssid_scanner


class TestScanner(unittest.TestCase):
    """
    Tests for ssid_scanner.
    """

    def test_check_for_invalid_adapter(self):
        """
        Assert that checking for the availability of an invalid wireless adapter returns false
        :return:
        """
        self.assertFalse(ssid_scanner.SsidScanner.adapter_is_available('9999999'))

    def test_return_adapter_name(self):
        """
        If a wifi adapters is enabled, a the name of the adapter should be returned.
        :return:
        """
        adapter_name = ssid_scanner.SsidScanner.get_available_adapter()

        self.assertIsInstance(adapter_name, str)


if __name__ == '__main__':
    unittest.main()
